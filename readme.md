# Documentation projet n-tiers

## Technologies utilisées

Côté client, j'ai utilisé le framework VueJS avec une surcouche Typescript qui est beaucoup pratique et complète. VueJS est un des trois plus gros framework JS actuellement avec Angular et React et est continuellement en évolution. Je l'ai choisi pour sa simplicité et sa rapidité de mise en place, mais également car je l'ai déjà utilisé dans plusieurs projets personnels.

Pour le serveur, j'ai utilisé NodeJS avec encore une fois l'ajout de Typescript. NodeJS est la technologie la plus utilisée en ce moment pour mettre en place des API rapidement et permet d'avoir le même langage côté client et serveur. De nouveau, j'ai eu l'occasion d'utilisé Node pendant mes stages ou pour des projets extérieurs, ce qui m'a conforté dans mon choix.

Pour finir, j'ai choisi MySQL pour la base de donnée, car je préfère utilisé du SQL pour sa fiabilité. Cette base comme le reste du serveur est hébergée par Heroku sur l'adresse suivante : https://tse-architecture.herokuapp.com/

## Client

### Structure

L'utilisation de Vue CLI permet d'avoir une architecture lisible et modulable dans un projet Vue. On trouve à la racine les différents fichier de configuration JSON, que ce soit pour npm ou pour Typescript. Le fichier *.env.dist* contient toutes les variables d'environnement à renseigner dans un fichier *.env* qui n'est pas versionné par Git par soucis de sécurité. Le dossier public contient les images et logo utilisés, 

Vue utilise le langage JS (TS ici), HTML et CSS pour produire des sites responsive et facilement customisable. Avec l'ajout de vuetify, des classes préfaite permette de faire une mise en page facilement et de choisir le thème du site.

On retrouve des dossiers assez commun dans des frameworks:

- Un dossier Vue qui comprend les pages auxquelles l'utilisateur peut accéder via un url
- Un dossier Component qui contient des morceaux de page customisable qui sont combinés dans les vues
- Un dossier Service qui contient toutes les fonctions d'appel au serveur. Les requêtes sont faites avec l'aide d'Axios
- Les autres dossiers sont spécifique à Vue et permettent de configurer le framework, notamment le routeur et 



### Démarrage

Pour installer tous les modules du projet, il est nécessaire d'avoir Node v>14.0 d'installer, il faut ensuite taper la commande *npm i* dans un terminal à la racine du projet. Cette commande va créer un dossier node_module qui contient toutes les dépendances. deux solutions sont ensuite envisageable:

-  Démarrer un serveur client indépendant avec la commande *npm run serve* qui va se lancer en localhost sur le port spécifié dans le fichier *.env*
- Construire et minifier le projet avec la commande *npm run build* . Un dossier *dist* va alors être créé avec les fichiers optimisés, ce dossier peut ensuite être servi par un serveur en ressource statique. Ce dossier doit ensuite être renommé *public* et déplacé à la racine du serveur.

## Serveur

Le serveur est donc lui aussi coder en typescript avec un découpage classique dans une structure n-tier :

- Un dossier *controller*, qui gère le routeur, et combine aussi la logique des services
- Un dossier *model* contenant la définition des classes en base de données
- Un dossier *dal* qui contient toutes les fonctions d'appel à la BDD
- Un dossier *core* qui contient le démarrage du serveur, la gestion des erreurs et la connexion à la BDD

La base de donnée est une base MySQL hébergée par JawsDB. 

### Démarrage

Là encore, les modules nécessaire doivent être installés avec la commande *npm i*. Il faut ensuite transcompiler le typescript vers du JS avec la commande *npm run build* ce qui va créer un dossier dist. Le serveur peut ensuite être lancer avec *npm run start*

Le serveur peut également être démarrer en mode "hot reloading" ce qui permet de le recharger dès qu'un changement est fait sans devoir recompiler. Il suffit pour ça d'utiliser *npm run watch*.

## Interface client

En arrivant sur l'interface sans être connecté, aucun servie ne sera accessible. La page de connexion est accessible depuis le bouton en haut à droite. Depuis l'écran de login, il est également possible de créer un compte en renseignant les informations de l'utilisateur.

Une fois connecté, différents onglets sont accessibles en fonction de du rôle de l'utilisateur. Un administrateur aura accès à tous les stages créés ainsi qu'a la map monde avec la répartition des étudiants.

Un élève pourra quand à lui créer un nouveau stage mais également consulter et modifier les stages qu'ils a déjà demandés. Les demandes peuvent être supprimées par l'élève ou par un administrateur. Lors de la création, une plage de date est demandé et la date ne peux bien entendu pas être dans le passé. Une auto-complétion des noms de pays est également disponible grâce à la base de données accessible de amcharts -> https://www.amcharts.com/. Ce plugin javascript a également permis de réaliser l'affichage du planisphère dans la parti administrateur.

Pour rappel, le projet est hébergé par la plateforme Heroku tout comme la base de données, accessible à l'url https://tse-architecture.herokuapp.com/. Heroku permet de facilement et rapidement déployer une application web avec l'aide git, et permet également de mettre en place une pipeline de tests ainsi que des analyses de statistique du site gratuitement.
