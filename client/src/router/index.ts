import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Home from '../views/Home.vue'
import store from '@/store/index'
import constants from '@/constants'


Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Home',
    component: Home
  }, 
  {
    path: '/login',
    name: 'Login',
    component: () => import("../views/SignIn.vue")
  },
  {
    path: '/register',
    name: 'Register',
    component: () => import("../views/SignUp.vue")
  },
  {
    path: '/internship',
    name: 'Internship',
    meta: {requiresAuth: true},
    component: () => import("../views/Internship.vue")
  },
  {
    path: '/internship/:uid',
    name: 'InternshipDetails',
    meta: {requiresAuth: true},
    component: () => import("../views/Internship.vue")
  },
  {
    path: '/myinternships',
    name: 'MyInternships',
    meta: {requiresAuth: true},
    component: () => import("../views/MyInternships.vue")
  },
  {
    path: '/internships',
    name: 'Internships',
    meta: {requiresAuth: true, isAdmin: true},
    component: () => import("../views/Internships.vue")
  },
  {
    path: '/map',
    name: 'Map',
    meta: {requiresAuth: true, isAdmin: true},
    component: () => import("../views/Statistics.vue")
  },
  {
    path: '**',
    name: 'Not Found',
    component: () => import("../views/NotFound.vue")
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach(async (to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!store.state.role) {
      next({
        path: '/login',
        params: { nextUrl: to.fullPath }
      });
    } else {
      const user = store.state.user;
      if (to.matched.some(record => record.meta.isAdmin)) {
        if (user && user.role === constants.ROLE_ADMIN) {
          next();
        } else {
          next(false);
        }
      } else {
        next();
      }
    }
  } else if (to.matched.some(record => record.meta.guest)) {
    if (!store.state.role) {
      next();
    } else {
      next(false);
    }
  } else {
    next();
  }
});


export default router
