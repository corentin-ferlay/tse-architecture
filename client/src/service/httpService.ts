import axios from 'axios';
import store from '@/store/index'
import router from '@/router';

axios.defaults.baseURL = process.env.VUE_APP_SERVER_URL;
export class HttpService {

    public post(url: string, data: Record<string, any>) {
        return axios.post(url, data, {
            headers: this.getHeaders()
        });
    }

    public get(url: string) {
        return axios.get(url, {
            headers: this.getHeaders()
        });
    }

    public patch(url: string, data: Record<string, any>) {
        return axios.patch(url, data,{
            headers: this.getHeaders()
        });
    }

    public delete(url: string) {
        return axios.delete(url, {
            headers: this.getHeaders()
        });
    }

    private getHeaders() {
        const headers = {
            'Authorization': 'Bearer ' + store.state.token,
        }
        return headers
    }

}

axios.interceptors.response.use((response) => {
    // Return a successful response back to the calling service
    return response;
}, async (error) => {
    // Return any error which is not due to authentication back to the calling service
    if (error.response.status !== 401 || error.config.url == '/users/login' || error.config.url == '/users/create') {
        return new Promise((resolve, reject) => {
            reject(error);
        });
    }

    store.commit('setToken', null)
    router.replace('/login')
    return new Promise((resolve, reject) => {
        reject(error);
    });
});




export default new HttpService()
