import { UserCreation } from '@/model/User';
import httpService, { HttpService } from './httpService';

export class UserService {
    constructor(private _httpService: HttpService) {

    }

    public signIn(email: string, password: string) {
        return this._httpService.post('/users/login', { email, password });
    }

    public signUp(user: UserCreation) {
        return this._httpService.post('/users/create', user);
    }

    public getUserInfo(uid: string) {
        return this._httpService.get('/users/' + uid)
    }

}

export default new UserService(httpService);
