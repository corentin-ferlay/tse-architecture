import httpService, { HttpService } from './httpService';

export class InternshipService {
    constructor(private _httpService: HttpService) {

    }

    public createInternship(data: Record<string, any>) {
        return this._httpService.post('/internships', data)
    }

    public deleteInternship(uid: string) {
        return this._httpService.delete('/internships/' + uid)

    }

    public updateInternship(uid: string, data: Record<string, any>) {
        return this._httpService.patch('/internships/' + uid, data)
    }

    public getInternshipsByQueries(fields: string[], values: string[]) {
        if (fields.length !== values.length) return
        let query = ""
        for (let i = 0; i < fields.length; i++) {
            if (i > 0) {
                query += "&"
            }
            query += `${fields[i]}=${values[i]}`
        }
        return this._httpService.get(`/internships/by${query?'?'+query: ''}`)
    }

    public getInternshipsByUserId(userId: string) {
        return this._httpService.get('/internships/by/userid/' + userId)
    }

    public getInternshipById(uid: string) {
        return this._httpService.get('/internships/by/id/' + uid)
    }

    public getInternshipsPerCountry() {
        return this._httpService.get('/internships/countries')
    }
}

export default new InternshipService(httpService)