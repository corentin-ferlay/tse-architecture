export interface UserCreation {
    email: string;
    firstname: string;
    lastname: string;
    promotion: string;
    password?: string;
}

export interface User extends UserCreation{
    uid: string;
    role: string;
}