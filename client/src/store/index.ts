import Vue from 'vue'
import Vuex from 'vuex'
import JwtDecode from 'jwt-decode';
import {User} from '../model/User'

Vue.use(Vuex)
export interface CustomState {
  role: string | null;
  user: User | null;
  token: string | null;
}

export default new Vuex.Store({
  state: {
    token: null,
    user: null,
    role: null
  } as CustomState,
  mutations: {
    setToken(state, newToken: string | null) {
      state.token = newToken
      if(newToken) {
        const decoded: any = JwtDecode(newToken);
        state.user = decoded.user
        state.role = decoded.user.role
      } else {
        state.user = null
        state.role = null
      }
    }
  },
  actions: {
  },
  modules: {
  }
})
