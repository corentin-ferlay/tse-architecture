export default {
    ROLE_USER: "USER",
    ROLE_ADMIN: "ADMIN",
    LS_JWT: "jwt",
    LS_REFRESH_TOKEN: "refreshToken"
}