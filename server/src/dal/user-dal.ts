import { promises } from "fs-extra";
import {OkPacket, Pool, RowDataPacket} from "mysql2/promise";
import {getPool} from "../core/db-connection"
import {TokenUser, User} from "../model/User"

interface UserDb extends User, RowDataPacket {

}

export class UserDal {
    private pool: Pool
    constructor(pool: Pool) {
        this.pool = pool
    }

    public async createUser(user: User): Promise<number> {
        const [dbRes] = await this.pool.query<OkPacket>("INSERT INTO users(uid, password, firstname, lastname, email, promotion, role) VALUES(?,?,?,?,?,?,?)",
        [user.uid, user.password, user.firstname, user.lastname, user.email, user.promotion, user.role])
        return dbRes.insertId
    }

    public async findUsersByFirstname(firstname: string): Promise<TokenUser[]>{
        const [dbRes] = await this.pool.query<UserDb[]>("SELECT uid, firstname, lastname, promotion, email, role FROM users WHERE firstname = ?", [firstname])
        return dbRes
    }

    public async findUsersByLastname(lastname: string): Promise<TokenUser[]>{
        const [dbRes] = await this.pool.query<UserDb[]>("SELECT uid, firstname, lastname, promotion, email, role FROM users WHERE lastname = ?", [lastname])
        return dbRes
    }

    public async findUserById(uid: string): Promise<User>{
        const [dbRes] = await this.pool.query<UserDb[]>("SELECT * FROM users WHERE uid = ?", [uid])
        return dbRes[0]
    }

    public async findUserByEmail(email: string): Promise<User>{
        const [dbRes] = await this.pool.query<UserDb[]>("SELECT * FROM users WHERE email = ?", [email])
        return dbRes[0]
    }

    public async findUsersByPromotion(promotion: string): Promise<TokenUser[]> {
        const [dbRes] = await this.pool.query<UserDb[]>("SELECT uid, firstname, lastname, promotion, email, role FROM users WHERE promotion = ?", [promotion])
        return dbRes
    }

    public async updateUserBasicById(uid: string, firstname: string, lastname: string, email: string): Promise<void> {
        await this.pool.query("UPDATE users SET firstname = ?, lastname = ?, email = ? WHERE uid = ?", [firstname, lastname, email, uid])
    }

    public async updateUserAdminById(uid: string, role: string, promotion: string): Promise<void> {
        await this.pool.query("UPDATE users SET role = ?, promotion = ? WHERE uid = ?", [role, promotion, uid])
    }

    public async deleteUserById(uid: string): Promise<void> {
        await this.pool.query<OkPacket>("DELETE FROM users WHERE uid = ?", [uid])
    }

}

export default new UserDal(getPool())