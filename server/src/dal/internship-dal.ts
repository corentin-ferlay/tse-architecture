import { promises } from "fs-extra";
import { date } from "joi";
import { OkPacket, Pool, RowDataPacket } from "mysql2/promise";
import Dal from "../core/dal";
import { getPool } from "../core/db-connection"
import { Internship } from "../model/Internship";

interface IntershipDb extends Internship, RowDataPacket {

}

interface TotalCountries extends RowDataPacket {
    country: string,
    total: number
}

export class InternshipDal {

    constructor(private pool: Pool) {
    }

    public async createInternship(intership: Internship): Promise<number> {
        const [dbRes] = await this.pool.query<OkPacket>("INSERT INTO internships(uid, city, country, startDate, endDate, userId, description) VALUES (?,?,?,?,?,?,?)",
            [intership.uid, intership.city, intership.country, intership.startDate, intership.endDate, intership.userId, intership.description])
        return dbRes.insertId
    }

    public async findInternshipById(uid: string): Promise<Internship> {
        const [dbRes] = await this.pool.query<IntershipDb[]>("SELECT * FROM internships WHERE uid = ?", uid);
        return dbRes[0]
    }

    public async findInternshipsByQueries(queries: Record<string, any>): Promise<Internship[]> {
        if(Object.keys(queries).length === 0) {
            const [dbRes] = await this.pool.query<IntershipDb[]>("SELECT i.*, u.firstname, u.lastname, u.email FROM internships as i JOIN users as u ON u.uid = i.userId")
            return dbRes
        }
        const [dbRes] = await this.pool.query<IntershipDb[]>("SELECT * FROM internships WHERE city LIKE ? AND country LIKE ? AND startDate >= ? AND endDate >= ?",
            [queries.city || '%', queries.country || '%', queries.startDate || new Date(1900), queries.endDate || new Date(1900)])
        return dbRes
    }

    public async findInternshipByUserId(uid: string): Promise<Internship[]> {
        const [dbRes] = await this.pool.query<IntershipDb[]>("SELECT i.* FROM internships as i JOIN users as u ON u.uid = i.userId WHERE u.uid = ?", [uid]);
        return dbRes
    }

    public async findInternshipsByCountries(): Promise<any> {
        const [dbRes] = await this.pool.query<TotalCountries[]>("SELECT country as id, count(*) as value FROM heroku_a1d3fa68a5f7367.internships GROUP BY country")
        return dbRes
    }

    public async updateInternshipById(uid: string, description?: string) {
        await this.pool.query<OkPacket>("UPDATE internships SET description = ? WHERE uid = ?", [description, uid])
    }

    public async deleteInternshipByID(uid: string): Promise<void> {
        await this.pool.query<OkPacket>("DELETE FROM internships WHERE uid = ?", [uid])
    }

}

export default new InternshipDal(getPool())