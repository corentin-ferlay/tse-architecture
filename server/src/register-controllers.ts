import studentController from './controller/users-controller'
import internshipsController from './controller/internships-controller'

export default [
    studentController,
    internshipsController
]