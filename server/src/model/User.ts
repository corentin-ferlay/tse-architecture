export interface TokenUser {
    uid: string
    email: string
    firstname: string
    lastname: string
    promotion: string
    role: string
    birthdate: Date
}

export interface User extends TokenUser {
    password: string
}