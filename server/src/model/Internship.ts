export interface Internship {
    uid: string,
    userId: string,
    city: string,
    country: string,
    startDate: Date,
    endDate: Date,
    description?: string
}