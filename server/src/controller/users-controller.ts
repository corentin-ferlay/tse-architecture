import { Controller } from "../core/controller";
import { isAuthorized } from '../middleware/auth-middleware'
import { Request, Response, Express, NextFunction } from "express";
import { SignOptions, Secret, sign } from "jsonwebtoken";
import userDal, { UserDal } from "../dal/user-dal";
import { isUserCorrect, isUUID } from "../validator";
import { TokenUser, User } from "../model/User"
import { v4 as uuid } from "uuid"
import * as cfg from "../config"
import { compare, hash } from "bcryptjs";
import { ConflictError, ForbiddenError, UnauthorizedError } from "../core/error/http-error";

export class UsersController implements Controller {

    initRoutes(app: Express): void {
        app.post('/api/users/login',
            (req, res, next) => this.login(req, res, next).catch(next))
        app.get('/api/users/:uid',
            (req, res, next) => isAuthorized(req, res, next).catch(next),
            (req, res, next) => this.findUserById(req, res, next).catch(next))
        app.patch('/api/users/:uid',
            (req, res, next) => isAuthorized(req, res, next).catch(next),
            (req, res, next) => this.updateUser(req, res, next).catch(next))
        app.post('/api/users/create',
            (req, res, next) => this.createUser(req, res, next).catch(next))

    }

    constructor(private userDal: UserDal) {
    }

    public async login(req: Request, res: Response, next: NextFunction) {
        const email = req.body.email
        const password = req.body.password
        const user = await this.userDal.findUserByEmail(email)
        if(!user) {
            throw new UnauthorizedError()
        }
        const samePass = await compare(password, user.password)
        if(!samePass) {
            throw new UnauthorizedError()
        }
        const token = await this.createJwt(user)
        res.send({ token })
    }

    public async findUserById(req: Request, res: Response, next: NextFunction) {
        const uid = isUUID(req.params.uid)
        const users = await this.userDal.findUserById(uid)
        res.send({ users })
    }

    public async createUser(req: Request, res: Response, next: NextFunction) {
        let user = isUserCorrect(req.body)
        const userExists = await this.userDal.findUserByEmail(user.email)
        if(userExists) {
            throw new ConflictError("Email already exists")
        }
        user.uid = uuid()
        user.role = 'USER'
        user.password = await hash(user.password, 10)
        await this.userDal.createUser(user)
        res.send({uid: user.uid})
    }

    public async updateUser(req: Request, res: Response, next: NextFunction) {
        const uid = isUUID(req.params.uid)
        if(uid!=req.user.uid && req.user.role != "ADMIN") {
            throw new ForbiddenError()
        }
        await this.userDal.updateUserBasicById(uid, "", "", "")
        res.send({})
    }

    private createJwt(user: User): Promise<string> {
        return this.signJwt({
            user: {
                uid: user.uid,
                firstname: user.firstname,
                lastname: user.lastname,
                promotion: user.promotion,
                role: user.role,
                email: user.email
            }
        },
            cfg.getjwtPassword(),
            {
                expiresIn: '1h'
            });
    }

    private signJwt(payload: object, secret: Secret, options: SignOptions): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            sign(payload, secret, options, (err: Error | null, encoded?: string) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(encoded);
                }
            });
        });
    }

}

export default new UsersController(userDal)