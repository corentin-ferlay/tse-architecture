import { Controller } from "../core/controller";
import { isAuthorized } from '../middleware/auth-middleware'
import { Request, Response, Express, NextFunction } from "express";
import { isInternshipCorrect, isInternshipQueryCorrect, isInternshipUpdateCorrect, isUUID } from "../validator";
import { v4 as uuid } from "uuid"
import internshipDal, { InternshipDal } from "../dal/internship-dal";
import { ForbiddenError, NotFoundError } from "../core/error/http-error";

export class InternshipsController implements Controller {

    initRoutes(app: Express): void {
        app.get('/api/internships/by/id/:uid',
            (req, res, next) => isAuthorized(req, res, next).catch(next),
            (req, res, next) => this.findInternshipById(req, res, next).catch(next))
        app.get('/api/internships/by/userid/:uid',
            (req, res, next) => isAuthorized(req, res, next).catch(next),
            (req, res, next) => this.findInternshipByUserId(req, res, next).catch(next))
        app.get('/api/internships/by',
            (req, res, next) => isAuthorized(req, res, next, ['ADMIN']).catch(next),
            (req, res, next) => this.findInternshipByQuery(req, res, next).catch(next))
        app.get('/api/internships/countries',
            (req, res, next) => isAuthorized(req, res, next).catch(next),
            (req, res, next) => this.findIntershipsPerCountry(req, res, next).catch(next))
        app.post('/api/internships',
            (req, res, next) => isAuthorized(req, res, next).catch(next),
            (req, res, next) => this.createInternship(req, res, next).catch(next))
        app.patch('/api/internships/:uid',
            (req, res, next) => isAuthorized(req, res, next).catch(next),
            (req, res, next) => this.updateInternship(req, res, next).catch(next))
        app.delete('/api/internships/:uid',
            (req, res, next) => isAuthorized(req, res, next).catch(next),
            (req, res, next) => this.deleteInternship(req, res, next).catch(next))

    }

    constructor(private internshipDal: InternshipDal) {
    }

    public async findInternshipById(req: Request, res: Response, next: NextFunction) {
        const uid = isUUID(req.params.uid)
        const internships = await this.internshipDal.findInternshipById(uid)
        res.send({ internships })
    }

    public async findInternshipByUserId(req: Request, res: Response, next: NextFunction) {
        const uid = isUUID(req.params.uid)
        const internships = await this.internshipDal.findInternshipByUserId(uid)
        res.send({ internships })
    }

    public async findInternshipByQuery(req: Request, res: Response, next: NextFunction) {
        const queries = isInternshipQueryCorrect(req.params)
        const internships = await this.internshipDal.findInternshipsByQueries(queries)
        res.send({ internships })
    }

    public async findIntershipsPerCountry(req: Request, res: Response, next: NextFunction) {
        const countries = await this.internshipDal.findInternshipsByCountries()
        res.send(countries)
    }

    public async createInternship(req: Request, res: Response, next: NextFunction) {
        const internship = isInternshipCorrect(req.body)
        const uid = uuid()
        internship.uid = uid
        internship.userId = req.user.uid
        await this.internshipDal.createInternship(internship)
        res.send({ uid })
    }

    public async updateInternship(req: Request, res: Response, next: NextFunction) {
        const update = isInternshipUpdateCorrect(req.body)
        const {uid} = req.params
        await this.internshipDal.updateInternshipById(uid, update.description)
        res.send({uid})
    }

    public async deleteInternship(req: Request, res: Response, next: NextFunction) {
        const uid = isUUID(req.params.uid)
        const internship = await this.internshipDal.findInternshipById(uid)
        if (!internship) {
            throw new NotFoundError()
        }
        if (internship.uid != req.user.uid && req.user.role != 'ADMIN') {
            throw new ForbiddenError()
        }
        await this.internshipDal.deleteInternshipByID(uid)
        res.send({ uid })
    }

}

export default new InternshipsController(internshipDal)