import {User} from './model/User'
import { UnprocessableEntityError } from './core/error/http-error';
import validators from 'validator'
import Joi from 'joi'
import { Internship } from './model/Internship';

export function isUserCorrect(body: any): User {

    const object = Joi.object({
        email: Joi.string().email().required(),
        firstname: Joi.string().min(2).max(100).required(),
        lastname: Joi.string().min(2).max(100).required(),
        promotion: Joi.string().min(3).max(20).required(),
        birthdate: Joi.date(),
        password: Joi.string().required()
    })
    let result = object.validate(body,{allowUnknown: true});
    if (result.error) {
        throw new UnprocessableEntityError()
    }
    return result.value;

}

export function isUUID(token: string): string {
    if (!validators.isUUID(token)) {
        throw new UnprocessableEntityError()
    }
    return token
}

export function isInternshipUpdateCorrect(body: any): Internship {
    const object = Joi.object({
        description: Joi.string()
    })
    let result = object.validate(body);
    if (result.error) {
        throw new UnprocessableEntityError()
    }
    return result.value;
}

export function isInternshipQueryCorrect(params: any): User {

    const object = Joi.object({
        city: Joi.string().min(2).max(100),
        country: Joi.string().length(2),
        startDate: Joi.date(),
        endDate: Joi.date()
    })
    let result = object.validate(params);
    if (result.error) {
        throw new UnprocessableEntityError()
    }
    return result.value;

}

export function isInternshipCorrect(body: any): Internship {
    const object = Joi.object({
        city: Joi.string().min(3).max(100).required(),
        country: Joi.string().length(2).required(),
        startDate: Joi.date().min(new Date()).required(),
        endDate: Joi.date().min(new Date()).required(),
        description: Joi.string()
    })
    let result = object.validate(body);
    if (result.error) {
        throw new UnprocessableEntityError()
    }
    return result.value;
}
