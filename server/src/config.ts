import dotenv from 'dotenv';

dotenv.config()

export function getDbUri(): string {
    return process.env.DATABASE_URI || "";
}

export function databaseName(): string {
    return process.env.DATABASE_NAME || "";
}

export function databaseHost(): string {
    return process.env.DATABASE_HOST || "localhost";
}

export function databaseUser(): string {
    return process.env.DATABASE_USER || "";
}

export function databasePassword(): string {
    return process.env.DATABASE_PASSWORD || "";
}

export function getServerPort(): string {
    return process.env.PORT || '8080'
}

export function getjwtPassword(): string {
    return process.env.JWT_PASSWORD || ""
}