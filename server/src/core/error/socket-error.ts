export class SocketError extends Error {
    constructor(private _status: number, private _code: string, private _title: string, private _detail: string = '') {
        super()
    }

    get status() {
        return this._status;
    }

    get code() {
        return this._code;
    }

    get title() {
        return this._title;
    }

    get detail() {
        return this._detail;
    }
}

export class BadAuthentication extends SocketError {
    public constructor(message: string = "Unauthorized") {
        super(401, 'unauthorized', message);
    }
}