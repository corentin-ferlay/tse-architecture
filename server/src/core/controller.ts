import { Express } from 'express'

export interface Controller {
    initRoutes(expressApp: Express): void
}