import { Pool } from 'mysql2/promise';
import { getPool } from './db-connection';

export default abstract class Dal {

    protected getPool (): Pool {
        return getPool();
    }

}
