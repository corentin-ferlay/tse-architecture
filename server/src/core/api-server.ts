import express, { Router, Request, Response, Express, NextFunction } from 'express';
import registerControllers from '../register-controllers'
import { Controller } from './controller';
import { InternalServerError, HttpError } from './error/http-error';
import helmet from 'helmet';
import cors from 'cors'
import * as cfg from '../config'
import path from "path"
import http from "http"

export class ApiServer {


    public constructor(private controllers: Controller[]) {
    }

    public run() {
        const app = express()
        const server = http.createServer(app)
        app.use(helmet())
        app.use(cors())
        app.use(express.json());
        app.use(express.urlencoded({ extended: true }))
        app.use(express.static(path.join(path.resolve(), 'public')))

        this.registerControllers(app)
        app.get('*', (req, res) => {
            res.sendFile(path.join(path.resolve(), '/public/index.html'));
        });
        app.use((err: any, req: Request, res: Response, next: NextFunction) => {
            if (err instanceof HttpError) {
                res.status(err.status).send(err.message).end()
            } else {
                console.error(err)
                res.status(500).send(new InternalServerError())
            }
        })

        server.listen(cfg.getServerPort(), function () {
            console.log(`App listening on port ${cfg.getServerPort()}`)
        })
    }

    private registerControllers(app: Express): void {
        for (const controller of this.controllers) {
            controller.initRoutes(app)
        }
    }

}

export default new ApiServer(registerControllers)
