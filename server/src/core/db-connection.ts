import { createPool, Pool } from 'mysql2/promise';
import * as cfg from '../config';

let pool: Pool;

export function getPool () {
    if (!pool) {
        pool = createPool({
            uri: cfg.getDbUri(),
            connectionLimit: 10,
            queueLimit: 10
        });
    }
    return pool;
}
