import { JsonWebTokenError, verify } from 'jsonwebtoken'
import { Request, NextFunction } from 'express'
import { UnauthorizedError, BadRequestError, ForbiddenError } from '../core/error/http-error'
import * as cfg from '../config'
import { TokenUser } from '../model/User'

export interface DecodedJwt {
    user: TokenUser,
    iat: number,
    exp: number
}

export async function isAuthorized(req: any, res: any, next: NextFunction, roles: string[] = ['USER', 'ADMIN']) {
    let token = req.headers['authorization']?.split("Bearer ")
    if (!token || token.length < 2) {
        throw new UnauthorizedError()
    }
    let decoded: DecodedJwt
    const cert = cfg.getjwtPassword()
    try {
        decoded = verify(token[1], cert) as DecodedJwt;
        if (!roles.includes(decoded.user.role)) {
            throw new ForbiddenError()
        }
        req.user = decoded.user
        next()
    } catch (err) {
        throw err
    }
}